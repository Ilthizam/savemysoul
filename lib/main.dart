import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/services.dart';

import 'package:fancy_bottom_navigation/fancy_bottom_navigation.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(new MaterialApp(
    title: "Rotation Demo",
    debugShowCheckedModeBanner: false,
    home: new SendSms(),
  ));
}

class SendSms extends StatefulWidget {
  @override
  _SendSmsState createState() => new _SendSmsState();
}

class _SendSmsState extends State<SendSms> {
  static const platform = const MethodChannel('sendSms');

  var currentPage;

  String message, contactNumber;

  String _timeString;

  Future<Null> sendSms(String message, String contactNumber) async {
    print("SendSMS");
    try {
      final String result =
          await platform.invokeMethod('send', <String, dynamic>{
        "phone": contactNumber,
        "msg": message,
      }); //Replace a 'X' with 10 digit phone number
      print(result);
    } on PlatformException catch (e) {
      print(e.toString());
    }
  }



  @override
  void initState() {
    _timeString = _formatData(DateTime.now());
    Timer.periodic(Duration(seconds: 1), (Timer t) => _getTime());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
//    return new Material(
//      child: new Container(
//        alignment: Alignment.center,
//        child: new FlatButton(onPressed: () => sendSms(), child: const Text("Send SMS")),
//      ),
//    );

    return Scaffold(
//      backgroundColor: Color.fromRGBO(22, 0, 89, 40),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  TextField(
                      onChanged: (String value) {
                        message = value;
                        print(message);
                      },
                      decoration: InputDecoration(
                          hintText: "Enter message",
                          icon: Icon(Icons.message))),
                  SizedBox(
                    height: 10,
                  ),
                  TextField(
                      keyboardType: TextInputType.phone,
                      onChanged: (String value) {
                        contactNumber = value;
                      },
                      decoration: InputDecoration(
                          hintText: "Enter number", icon: Icon(Icons.dialpad))),
                ],
              ),
            ),
            Card(
              elevation: 10.0,
              margin: EdgeInsets.all(10.0),
              child: ListTile(
                contentPadding: EdgeInsets.all(8.0),
                title: Text(
                  "Send Message",
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
                trailing: Icon(Icons.send),
                selected: true,
                onTap: () => sendSms(message, contactNumber),
              ),
            ),
            Text(
              _timeString,
              style: TextStyle(fontSize: 50),
            ),
          ],
        ),
      ),
      bottomNavigationBar: FancyBottomNavigation(
          tabs: [
            TabData(iconData: Icons.list, title: "TO Do"),
            TabData(iconData: Icons.cake, title: "Birthday"),
            TabData(iconData: Icons.calendar_today, title: "Meeting")
          ],
          onTabChangedListener: (position) {
            setState(() {
              currentPage = position;
            });
          }),
    );
  }

  void _getTime() {
    final DateTime now = DateTime.now();
    final String formattedDateTime = _formatData(now);

    setState(() {
      _timeString=formattedDateTime;
    });

  }

  String _formatData(DateTime dateTime) {
    return DateFormat('MM/dd/yyyy hh:mm:ss').format(dateTime);
  }
}
